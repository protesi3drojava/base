# Meshmixer Manual ITA

Questo manuale è stato fatto con l'intento di insegnare le basi di Meshmixer solo nel contesto di questa repository: stampare protesi low-cost e low-skill, accessibili a tutti. Se si cerca una guida più dettagliata del programma, consultare il [manuale ufficiale](http://help.autodesk.com/view/MSHMXR/2019/ENU/) di Meshmixer.

## Importare Esportare Salvare

Il modo più semplice per importare un file 3D è attraverso il bottone **IMPORT** a forma di + nel menu a lato o un bottone simile quando si avvia il programma. L'alternativa è nel menu **File>Import**.

Quando si importa un ulteriore oggetto 3D al progetto viene chiesto di scegliere tra due opzioni: **APPEND** oppure **REPLACE**. APPEND significa che l'ulteriore oggetto verrà aggiunto alla scena, REPLACE significa invece che l'oggetto sostituirà il primo.

Per salvare il progetto andare su **File>Save** oppure premere **Ctrl+s**

I progetti verranno salvati nel formato **.mix**

Per aprire un progetto già iniziato andare su **File>Open** oppure premere **Ctrl+o**  e selezionare il file .mix desiderato.

I formati compatibili per l'importazione sono: .stl .obj .amf .ply .off .3mf .mix

I formati compatibili per l'esportazione sono: .stl .obj .amf .ply .3mf .dae ,wrl .smesh

I formati che importeremo saranno principalmente .obj

## Camera View

Per navigare dentro Meshmixer si possono usare i comandi di mouse e tastiera oppure il hotbox.

Ruotare l'inquadratura della scena (tumble): **PC**: Premere il tasto destro e trascinare il mouse. **MAC**: Tenere premuto il tasto sinistro del mouse e trascinare oppure premere il tasto destro del mouse assieme al tasto **ALT**

Spostare l'inquadratura (pan): **PC**: Trascinare il mouse tenendo premuta la rotella centrale. **MAC**: Premere il tasto destro del mouse assieme ai tasti **ALT**+**SHIFT**


Ingrandire o Ridurre la scena (zoom): Ruotare la rotella centrale del mouse o scorrere due dita sul trackpad

### Hotbox

Il Hotbox è un piccolo menu che appare quando si preme la barra spaziatrice. Qui trovate poche funzionalità utili a portata di mano.

<img src="img/01.jpg" width="30%"/>

## Strumenti Principali

#### Lista Oggetti

Cliccando sulla voce di menu **View>Show Objects Browser** si apre una finestra che mostra tutti gli oggetti nella scena. Si possono duplicare eliminare e nascondere gli oggetti.

#### Visualizza Wireframe

Per visualizzare il wireframe di triangoli cliccare su **View>Show Wireframe** oppure premere il tasto **w**.
È particolarmente utile quando si scolpisce e si vuole vedere la geometria dell'oggetto.

#### Selezionare

Selezionare è un'azione fondamentale e ha tante funzionalità al suo interno. Si seleziona cliccando il bottone **SELECT** a forma di freccia nel menu a lato oppure premendo il tasto **s** sulla tastiera.

<img src="img/06.png" width="200">

Ci sono tre modi per usare il Brush:

- **Unwrap**, seleziona una "chiazza" di superficie equivalente al tuo "pennello". La superficie deve essere continua/connessa;
- **Sphere**, seleziona ogni superficie all'interno di una sfera. Le superfici possono essere discontinue/disconnesse;
- **SphereDisc**, come Sphere ma seleziona solo superfici continue/connesse, è una via di mezzo.

Per **deselezionare** con Brush premere **Shift** o **Ctrl** mentre si usa Brush. Questo vale anche per Sphere e SphereDisc.

Nel pannello **Filters** ci sono tre opzioni per espandere la selezione:

- **Geodesic Distance**, la selezione prova ad espandersi in modo costante, è l'opzione più semplice;
- **Crease Distance**, la selezione prova ad espandersi evitando triangoli che formano spigoli appuntiti, favorisce l'espansione su superfici quasi-piane o "bombate";
- **Vertex Color Similarity**, non ci interessa al fine di questo manuale.

Per espandere la selezione si deve premere **Ctrl e il tasto destro del mouse mentre si sposta il cursore sullo schermo**.

Una volta creata una selezione, essa può essere oggetto di diverse azioni, infatti comparirà in cima ai pannelli di prima un ulteriore con dei menu. Qui di seguito vediamo le azioni di nostro interesse.

#### Trasformare

Per spostare o ruotare una selezione sugli assi **xyz** andare su **Deform...>Transform**. Trascina il mouse lungo le frecce per svolgere questa funzione. Un'altra opzione è andare su **Edit>Transform** se si vuole muovere l'oggetto o gli oggetti che si trovano dentro all'**Object Browser**.

<img src="img/23.png" width="200">

#### Cancellare

Semplicemente cancella la mesh selezionata, lasciando un buco.
Si cancella andando su **Edit...>Discard** o premendo il tasto **x**

#### Creare un FaceGroup

Un facegroup è una superficie a cui viene affibbiato un tag e un colore specifico sull'oggetto. In pratica sono delle selezioni che puoi salvare e riutilizzare anche dopo che hai chiuso il tool SELECT o anche dopo aver chiuso il programma.
Si creano facegroups andando su **Modify...>Create FaceGroup** o premendo i tasti **Ctrl+g**

#### Espandere la Selezione

Due modi particolarmente utili per espandere la selezione sono: espandere a tutta la superficie connessa, espandere a tutto il facegroup.

Per la prima opzione si va su **Modify...>Expand to Connected** o premendo il tasto **e**.
Per la seconda opzione si va su **Modify...>Expand to Groups** o premendo il tasto **g**.

<img src="img/08.png" height="200"> | <img src="img/09.png" height="200">
|:-:|:-:|
Selezionare parte del FaceGroup | Espandere a tutto il FaceGroup

#### Separare

La selezione diventa un nuovo oggetto, separato dall'originale.
Si separa andando su **Edit...>Separate** o premendo il tasto **y**.

#### Bordi Levigati

La selezione segue la mesh quindi in quasi ogni caso verrà una selezione molto spigolosa.
Si levigano i bordi della selezione andando su **Modify...>Smooth Boundary** o premendo il tasto **b**.

#### Estrudere Estrarre & Offset

Questi sono tre tool simili:

- Si estrude andando su **Edit...>Extrude** o premendo il tasto **d**.
- Si estrae andando su **Edit...>Extract** o premendo i tasti **Shift+d**.
- Si fa un offset andando su **Edit...>Offset** o premendo i tasti **Ctrl+d**.

Noi utilizzeremo principalmente offset. Offset crea una superficie in qualche modo parallela a quella di partenza.

<img src="img/11.png" height="200"> | <img src="img/10.png" height="200"> | <img src="img/12.png" height="200">
|:-:|:-:|:-:|
Estrudere | Estrarre | Offset

Come si vede dalle immagini, la superficie di offset può essere connessa o meno.

#### Levigare

Si leviga la selezione andando su **Deform...>Smooth** o premendo i tasti **Ctrl+f**.

#### Unire

Per unire si intende creare delle superfici che uniscono due bordi aperti ("buchi") selezionati. Per selezionare un bordo cliccare più volte su di esso con il tool SELECT.

Si uniscono due bordi andando su **Edit...>Join** o premendo il tasto **j**.


#### Tagliare

Per tagliare un oggetto si va su **Edit...>Plane Cut**. Nella casella di spunta trovate diverse opzioni. Potete per esempio decidere se tenere entrambi le parti dell'oggetto tagliato oppure solo una parte.

<img src="img/13.png" height="200">

#### Chiudere le spaccature

Per chiudere le spaccature si va su **Edit>Close Cracks**

#### Ispezione

L'inspector è molto utile e si trova andando su **Analysis>Inspector**. Questa funzionalità permette di fare un'ispezione veloce ai file che andranno poi a essere stampati con la stampante 3D. Infatti l'inspector identifica tutti gli elementi del file che possono creare problemi in stampa, permettendo quindi di correggere in tempo il file. Inoltre, piccoli errori possono essere corretti automaticamente.

<img src="img/04.jpg" width="30%"/>

#### Overhangs

Si possono creare dei supporti ottimizzati grazie all'opzione overhangs che si trova andando su **Analysis>Overhangs**.

<img src="img/03.jpg" width="30%"/>

## Esercizi

Per iniziare consigliamo di sperimentare le funzionalità sul coniglio già presente nel software.


<img src="img/02.jpg" width="30%"/>

#### Timbri

Per aggiungere i timbri andare su **Stamps**, cliccare su un timbro, cliccare sull’area dove va messo il timbro e trascinare il mouse.

#### Meshmixer

Dentro **Meshmixer** trovate diverse parti che possono essere aggiunte. 

<img src="img/14.png" width="200"/>

Se queste forme vengono trascinate sopra altri oggetti, appaiono queste opzioni:

<img src="img/15.png" width="100"/>| <img src="img/16.png" width="100"/>| <img src="img/17.png" width="100"/>| 
|:-:|:-:|:-:|
sposta|ruota|scala|

<img src="img/19.png" width="100"/>  <img src="img/18.png" width="100"/>| <img src="img/20.png" width="100"/>| <img src="img/21.png" width="100"/>|
|:-:|:-:|:-:|
cambiare opzioni|torsione|punta|
