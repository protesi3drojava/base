# CURA and OPENSCAD User Manual

## Introduction

Before to read these manuals, it is recommended to read the manual related to the printer (here we are talking about Creality CR-10) because it contents the description about the functioning of the printer and its mechanical parts. Basically, this kind of printer deposit plastic’s layers on a heated floor (bed) with a small heated nozzle (extruder), usually 0.4 mm, that ejects the melted plastic filament. In order to do this activity, the printer needs some instruction you provide through the software. 
To do so, this manual is structured to provide accurate information about the use of two software that are use respectively for slice and scale your objects in order to prepare them for the 3D printing and to have a good quality of production. 
The manual includes both general information of Cura software and specific indication related to the production of the prosthetics model Kwawu Arm Thermoformed 2.0 and Socket version (https://www.thingiverse.com/thing:2841281). Openscad is in a specific developer version to scale properly this kind of prosthesis. Both the software are free and open source.

IMMAGINE DELLA SEZIONE

## First steps with CURA

### 1.1 What it is and what it is needed for 

Cura is one of the available software for creating Gcode files, which are the ones 3D printers are able to read. In fact, creating a 3D model of the object we wish to print is not enough to actually print it. 
Given a .stl file, i.e. a file created using programs as Blender or Openscad that represents what we want to print, such as a glass, a keychain, etc., we have to specify to the 3D printer how we want to print it: its inclination, the temperature the filament has to be, etc. What we want to print is encoded in a .stl file; what we want to print and how, is encoded in a gcode file. See Figure 1.

FIG 1

To specify to Cura how we want to print, we must at first 

- specify the 3D printer and material we will be using. Then, we must 
- chose the .stl file/files we want to print (it is possible to print more objects at a time), 
- how to position the object(s) in the printer plate and then 
- specify the parameter values (i.e. layer height, extrusor speed, etc.).

In this manual it is shown how to use Cura: using an example it is described step by step how to transform a .stl file in a .gcode file (the specific word is slicing). One of the key points is the selection of values for the parameters. There are hundreds of parameters and the values are object-dependent, that is, there are no correct parameter values in general, and only user experience can help. Here we will list the most important parameters, what they do and what they affect. A good print is determined by the selection of those parameters. 
After all parameters are set, the 3d printer and filament type selected, pressing a simple button Cura will automatically proceed with the slicing (generate the .gcode file out of the specified inputs).

### 1.2 How: installation and first steps

Cura is a free software available at https://ultimaker.com/software/ultimaker-cura.
The version used in this manual is 4.0.0, but the main things are common among different versions, so it shall be easily applied to the version of Cura you will decide to install.
No requirements are needed for its installation.

### 1.2.1 Overview of CURA interface	

Once Cura is opened, it shall appear like in Figure 2. You can see in the top center, there are different possible views, the main are Prepare and Preview

FIGURA 2

Prepare view
The main view represents the printer table which, at the very start we can see it empty.

FIGURA 3

- Pressing the folder icon up left (see number 1 in Figure 3) it is possible to select the .stl file to import (it is possible to execute this action more than once if we want to print more than one object at a time).  The same action can be performed clicking File -> Open File(s) or pressing on the key-board Ctrl+O.
- Pressing the buttons in A (see Figure 3) it is possible to change the perspective of the plate. Moreover, using the mouse roller it is possible to zoom in and out, pressing keyboard buttons right/left up/down it is possible to rotate the perspective.
- Pressing the button situated in correspondence of number 2 in Figure 3 it is possible to select the 3d printer that will be used. There’s always a default printer selected. The same action can be performed clicking Settings -> Printer. If your 3d printer is not in the list of the selectable printers, you can add one pressing Add printer button. Choose one 3d printer from the list, then press Add printer. See Figure 4. There is also the advanced option to Manage printers, where the user can change 3d printer parameters, but we recommend to do not change them.

FIGURA 4

- Pressing the button situated in correspondence of number 3 in Figure 3 it is possible to select the filament material that will be used. There’s always a default material selected. The same action can be performed clicking Settings -> Extruder. 1. See Figure 4. From the same button you can select the nozzle diameter you are using SISTEMA QUESTA PARTE

(Here (insert link) you can find a table with the characteristics of main materials for 3D printer with their specifics necessary for setting CURA.)

FIGURA 5

- Pressing the pencil icon situated at the very right of the bar that is in correspondence with number 4 in Figure 3 it is possible to choose all the parameter values. There are always default parameter values selected/ the last ones used by the user. 
Details on parameters will be given is section […]
- It is possible to import a previously used and saved profile, in order to do not change settings on 3d printer/ material and parameter values. [IMPORT PROFILES]. Once the profile is imported, it is possible by the way to change some settings/ parameter values and possibly override the profile or save it with a different name. []. (SISTEMA LA PARTE SUI PROFILI FAI UN CAPITOLO A PARTE (ANCHE SUL SALVATAGGIO)

Once at least one object will be imported, at the bottom right part of the video it will appear the slice button. See Figure 5. If we click on one of the imported objects, button on the center left of the video are enabled. They are used to move, rotate, scale, mirror the selected object. Notice that if we import more than one .stl file, Cura automatically places it so to do not overlap with the ones already placed in the plate (notice that changing the order in which we import .stl files in Cura, the objects will be placed differently).

FIGURA 6

Since Cura contains default values for printer, material and parameters, once the object is imported the user can decide to transform it in .gcode file it, pressing the Slice button.
The execution of the slicing might take some moments, but once it will be done, it will be shown to the user how much time and how much material (grams and meters) will be needed for the completion of the printing. 
Once the slicing is done, if the user changes position of the object(s) or printer/material/parameter value the slicing will be needed again.
It is now possible to move to the Preview view.

Preview view
The aim of this view is to visualize how the object exactly will be created, that is also where the extruder will move, which helpers (or support) will be created automatically, etc. 
There are two further views that can be selected: Layer view and X-ray view.
In the Preview – Layer view view it is possible to simulate the extruder moves playing the start button at the bottom center, starting from the layer selected in the right center bar; see Figure 6.

FIGURA 7

In the Preview – X-ray view it is possible to see how the imported objects are made inside; see Figure 7. (RICONTROLLA SE E’ DA COMPLETARE

FIGURA 8

### 1.2.2 Materials matter: descriptions, characteristics and settings for CURA

NOTE: it is recommended to have a look at the Materials’ Manual (put link here) for all the detailed descriptions and indications about the printer’s setting and the software’s setting related to the materials’ characteristics that will affects your printing quality. 
Here you can find some useful indications only related to the parameters’ setting issues (not to the printer setting): indeed, one important element for your printing quality is the material you use. You need to set your material on CURA. 
CURA have already some default’s profiles with pre-setting for each material (PLA-PETG-ABS) but some materials may be not present. Furthermore, each brand of filaments has different extrusion temperature. To start, you need to know at least the recommended temperature of extrusion, the bed temperature and the diameter of your material, but other characteristics of the material will affect the results of you printing. Instead, to set another material profile on CURA (and to use new parameters not of default) you need to know also the density of the material (Fig. 9).

Figure 9
PLA, Polylactic acid, is the easiest to print and is also compostable (100% biodegradable) but at the same time it is less resistant. The main temperatures required for PLA are from 200° to max 220° (Fig. 10). It isn’t required other parameters specifications., it is recommended to use ever retraction (see below pg.) and the z-hop (see below, pg.) functions.
PETG is not biodegradable but is totally recyclable; it needs an higher temperature (Fig. 10), it is more difficult to print, in particular it have a level of viscosity that make hard-working identify quickly the correct parameters, furthermore each label of wire have different composition in its additives and this means that each label could have different optimal parameters you can find only with many experimentation (this is valid also for PLA but overall it’s more easy to find them). For a good print with PETG the retraction distance (see below pg.) needs an higher value and to disable z-hop is required (see below pg.).
TPU, Thermoplastic polyurethane, is any of a class of polyurethane plastics with many properties, including elasticity, transparency, and resistance to oil, grease and abrasion. With the Creality CR-10 printer it is good also 200° as extrusion temperature (but see however Fig. 10 - 11) and it’s required to disable retraction and z-hop function on Cura.


Figure 10

Figure 11 Source: https://blog.prusaprinters.org/advanced-filament-guide_39718/
If you have difficult to find the correct temperature (see here the troubleshooting commonly related to the temperature) you can use tests, recommended, for the temperature here. 
In the Fig. 12 below you can see some reaction of the filaments if they are treated with certain chemical material, remember that PLA is hydrophobic: it is do not like water and humidity.


Figure 12
CHAPTER TWO: MANAGE THE MAIN PARAMETERS
2.1 Setting parameters
The parameters are variables dependent from at least three factors: the material you use, the shape of the object and the position you will choose for your object on the plate. However also the speed of print that you are looking for or the definition of the details (i.e. the quality and the characteristics of the objects you are looking for) are determinant.

    • First, we describe the main parameters you will use to print correctly one prosthesis, 
    • then we put together in a table the associations between each parameter and a typical shape of characteristic pieces of a prosthesis, 
    • final, we explain how save profiles in your laptop: in this way you can choose your profile set previously or create a new profile after new experimentations.

2.1.2 Parameters classification and profiles 

(Source: https://ultimaker.com/en/resources/21932-mastering-cura) 
https://support.ultimaker.com/hc/en-us/sections/360003548619-Print-settings

The parameters are classified under 10 main labels: Quality, Shell, Infill, Material, Speed, Travel, Cooling, Support, Build Plate Adhesion, Special Modes (Fig. 13, red box; fig. 14, green boxes;). 

Quality refers mainly to the first basic parameters: the height of the layers and the width of the lines. Shell contents -mainly- parameters about the characteristics of the walls of the object. Infill is related to the filling of the object. Material contents parameters related to the printing material (e.g. the temperature and other). Speed: the name says it all, contents parameters for set the print speed of each part of the object. Travel is referred to the function used when the nozzle does not print but only move from one point to another. The Cooling section is about the fun’s functions. Support contents all the settings for make supports on your object. Build Plate Adhesion refers to some parameters useful to improve the adhesion of the objects to the bed. Finally, Special Mode contents parameters to allow in some cases to print the object one by one or other special functions. 


Figure 13

Under the box for the selection of the material you use (e.g. PLA) you can find the selection of profiles. The first time you use CURA you can choose the basic profile to start your trial, that is “Normal Quality 0,1 mm”, from this profile you have to check the default parameters are consistent with shape, position and features of your object (Fig. 14 orange box n. 2).
Clicking the gear icon next to “Quality” you can change your parameters in visible or non-visible (Fig. 14 orange box n. 1) so you can choose to maintain visible only the parameters you are able to manage or the most relevant for your daily work (Fig. 15 green boxes). At the end of this chapter you fill find instruction about how to save new profiles.



Figure 14	

Figure 15
2.1.3 The relation between parameters, object and printer: figure out where to start
Before starting with the parameters description, here we provide some useful notions of their meaning in relation to the 3d object and the mechanic features of the 3d printer: below we explain how works the printer and the functions of some main parameters in order to better understand the whole mechanism of print.
We know that the 3D printing process consist basically on deposit plastic material in layers (fig. 16 a). These layers are deposited one after another crushed on them (fig. 16 b) to form a solid in three dimensions and to obtain a copy of the 3d model in accordance with the instructions that have been given to the machine. 

Figure 16

On CURA interface, you will set parameters also related both to mechanics characteristics of your printer - e.g. when you set “line width” the normal is to set the same value of your nozzle diameter that is most commonly 0,4 mm (Fig. 17) – and to the filament (i.e. “Diameter” under the voice “Material”, see also below). 


Figure 17: three important parameters that are related each other are Nozzle Diameter, Extrusion Diameter (i.e. Flow) and Line Width. These parameters affect both the level of details on your objects and the time of printing. The Layer Height is your choice, but it affects the same features of your object.  
Speaking about the relation between your object and the setting of parameters, below you can see some graphical explanations of this relation: important are 
    • the shape
    • mechanical qualities required, (e.g. resistance, flexibility, lightness)
    • structural qualities (e.g. the thickness of the external walls), 
    • the time required
    • aesthetical qualities
Because the material is deposited in layers, if you need your object more resistant to traction and bending you need to look how position your object on the CURA plate (that is on the bed when the printer will start). For example, if you want to increase the resistance of the printed object to a force that is applied laterally on its length to break it, it will be necessary to place it so that it is lying on the plate, with its long side adjacent to the printing plane; in this way it increase the resistance even to longitudinal tractions. Here, in Fig.18 for example, you can see a 3D printed screw and for print this kind of objects resistant you have to position it horizontal on the plate.

Figure 18
Basic parameters to know o start: infill, wall thickness, supports
Before start with the detailed description of each parameters, other basic parameters to know are related to the basic structure of the object you can print: these are infill, wall thickness, supports . 
First there is the infill, that is the filling of the object (Fig. 19 a). You can choose the quantity of the material extruded in the inner part so you can decide the density of the filling that affects the weight, the resistance and the flexibility, but also the time of your print. More than this, you can select the shape of the layers that will form the inner filling of the object selecting also the typology of the pattern: if you want a strong object with not a full filling, that means a very long print and high weight, you may select Gyroid instead of Lines (weak) or also Cubic (strong) (Fig.19 c), for example (see also below pp. 17-18-19; fig.23 aggiorna pagine e immagini).

Figure 19
Another important feature is the thickness of the walls (fig. 19 b) that will affect the mechanical properties of your objects. You have to set a thickness of your walls otherwise you will obtain fragile objects or errors; wall thickness has to be ever multiple of the line width i.e. the nozzle diameter, Otherwise, with a thickness of the walls different from the diameter of the nozzle, during the printing process, the nozzle will make small overlaps between adjacent layers causing errors such as the formation of small steps.
Finally, you have to understand if and why you may need supports: the supports are structures that can sustain your print if you need to print surfaces without sustain (Fig. 20) as for example can be bridges or appendix, as arms in a digital miniaturized reproduction of a human body.
The supports need to be firmly anchored to the printer bed but easy to remove: to obtain this you need to set the Brim (see here pg.) and Support z distance (here pg.); more generally we recommended to study the parameters of supports (here p. ).


Figure 20
The nozzle 
One important features of the printer that will affect your printing is the nozzle’s diameter (Fig. 16-17). In general, nozzles are classified in the following ways:
    • Filament diameter: 1.75 mm or 2.85 mm
    • Diameter of the nozzle: The dimensions include 0.25 mm, 0.3 mm, 0.4 mm, 0.5 mm, 0.6 mm, 0.8 mm, 1.0 mm
    • Material with which the nozzle is made
    • Nozzle shape
    • Thread
    • Tree length
The most the nozzle is small the more your print will be detailed in all its small parts but, on the other hand, you will need more time to complete your print. If you need very sculpted and detailed features on your object it can be useful have a small nozzle but, if you need to print simple objects, the result will be good and more rapid with a normal nozzle (0,4 is the most common) or even with a large nozzle. When you buy any printer, you will normally find a 0.4 nozzle, this is the better compromise between details and print times. Other characteristics that you must take in consideration are that 1. Smaller diameter means less support surface for your layers so bigger nozzle means stronger objects; 2. Bigger diameter means more visibility of the layers deposited when your print is finished, and it needs high temperature. Generally, the assumptions below can be valid if we switch from a 0,4 mm nozzle to a 0,8 mm nozzle: 
    • higher temperature
    • less perimeter, so fewer walls (number)
    • less infill %
    • low speed
    • higher fan or 100%; on the first two layers at 0%
    • First layer height and generally layer height can be higher (the height of the layer can never be more than 80% of the perimeter of the nozzle which for 0,4 nozzle = 0,32 ; for a 0,8 nozzle diameter it can arrive at 0,64)
More information in the section ……. 
In all these manuals we use ever a filament diameter of 1,75. For the nozzle, we mainly use a brass nozzle with 0,4 mm of diameter; the first part of this guide is done using 0,4 nozzle but we provide information also to use a 0,8-nozzle diameter.
2.1.4 Description of main parameters
Below you can find the selection from the panel of almost all the parameters and in particular the most commonly used, with descriptions of their functions (see also above pag.10). 

Below, near the name of each parameters, we have put a mark in order to highlight the most important parameters that you have to manage for the firsts prints. The very basic parameters used to print simple objects (or some benchmarks) are marked by three stars ***; other parameters useful but that you will start to manage progressively in a second moment, because more complicated, are marked by two ** or, for the more complex treated here, one * stars. 

Note: all the parameters related to the double extruder are not included, for further information about this parameters or about other not included here visit https://support.ultimaker.com/hc/en-us/sections/360003548619-Print-settings or the website of Ultimaker.

Quality Settings: Settings that define the (visual) quality of the print. The layer height - one of the most often changed settings - is the thickness of one printed layer (in mm). With a thinner layer height, you will usually increase the quality of the print, leading to a smoother surface and more detail visible in the Z-direction (height) of the model. On the other hand, by using thicker layers you can decrease the print time substantially.

TABELLA PG 20

Shell Settings: Settings related to the outside of the print. Wall Thickness adjusts the thickness of outside walls (on the X/Y axis) of the model. This value divided by the wall line width defines the number of walls and is generally a multiple of the line width. For example: when using a wall line width of 0.35 mm, it will be logical to set the wall thickness to 1.05 mm (3 * 0.35), which means that 3 walls will be printed.
In general, a wall thickness of 2 or 3 times the line width is sufficient. A higher value will create a sturdier model and decreases the chance of leaks, while a lower value can significantly decrease the print time and filament costs.

TABELLA PG 22-29

Infill Settings—Settings related to the inside of the print. Infill density defines the amount of plastic used on the inside of the print. A higher infill density means that there’s more plastic on the inside of your print, leading to a stronger object. An infill density between 10% and 20% will be enough for most objects though.
Instead of filling in the infill density as a percentage, it’s also possible to set the line distance. This determines the distance between each infill line, which has the same effect as changing the fill density.

TABELLA 30

Material Settings—Settings related to Material. Retraction is one property that can be set. At the places in a print where the printer has to do a travel move between two printed parts and you don’t want it to leave the plastic in between the use of retraction is important. This means that the filament is pulled back by the feeder, so that it doesn’t leak from the nozzle during the travel moves. By using retraction, “stringing” (thin threads of plastic in between the printed parts) is prevented, resulting in a much cleaner final model. You have to be careful with flexible materials or models that require a lot of retractions though, as that might lead to grinding of the filament.

TABELLA 33-50

2.1.5 How to save profile and parameters
Import/export profile

https://ultimaker.com/en/resources/21932-mastering-cura 

>Impostare la stampante su cura (screenshot parametri) e il materiale di stampa 
>specifiche materiali

> comando > import file 
> posizionamento e comandi per il posizionamento*

> setting dei parametri: > scelta dei parametri visibili
> quali sono i parametri più importanti?
Su cosa influiscono (vedi anche paragrafo “troubleshooting” )
 
 - SETTING SPECIFICO PER MATERIALI E STAMPE
 
setting for PLA:

> settings fingers PLA
> parameters

> trouble
> note

>settings arm , cover and cuff PLA
> parameters

> trouble
> note

> settings for other pieces PLA

> settings for HINGE (TPU)

setting for PETG:

> settings fingers PETG
> parameters

> trouble
> note

>settings arm , cover and cuff PETG
> parameters

> trouble
> note

> settings for other pieces PETG

> settings for HINGE (TPU)


- TROUBLE SHOOTING (COSA FARE SE………….)

https://www.simplify3d.com/support/print-quality-troubleshooting/ 


MANUALE STAMPANTE

- AVVIARLA
- COMANDI DEL DISPLAY > A COSA SERVONO (MOVE AXIS)
- ALLINEAMENTO
- RISCALDAMENTO
- AVVIO STAMPA** (LACCA)
- CAMBIO FILO

- **COSA FARE SE….

- MANUTENZIONE STAMPANTE
> piano di manutenzione
> PULIZIA BED
> PULIZIA DEL NOZZLE (KIT)
