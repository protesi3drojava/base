# Dyeing of 3D-printed PETG and PLA objects

## Methods

A liquid dye for synthetic fabric is used to dye PLA and PETG-based 3D-printed components. The dyeing test reported in this section was carried out using [RitDye More Synthetic](https://www.ritdye.com/type/dyemore-for-synthetics/) liquid dye. If this product is not available, we recommend testing liquid or powder-based dyes specifically formulated for synthetic fabrics (polyester, acrylic, acetate, nylon, spandex, etc.). Prior to dyeing your 3D-printed pieces, we suggest repeating the test illustrated in this section with your dyeing product of choice, to evaluate the concentration, temperature and times required to obtain your preferred colour.

Due to the complexity of dyeing synthetics, Rit DyeMore must be diluted in hot water and the 3D-printed component dyed at high temperature. The dye was diluted in boiling water (T=100°C), and stirred for a few minutes to achieve complete dissolution of the product. Four different concentrations were tested (see Table 1):
- 2.5 ml (half teaspoon) of liquid dye in 0.3 l of water (0.8% concentration)
- 5 ml (1 teaspoon) of liquid dye in 0.3 l of water (1.6% concentration)
- 10 ml (2 teaspoons) of liquid dye in 0.3 l of water (3.2% concentration)
- 15 ml (3 teaspoons) of liquid dye in 0.3 l of water (4.8% concentration)

The solution was left to cool down until a defined temperature was reached: PLA samples were immersed in the diluted dyeing solution at 70-80°C for 30 minutes; PETG samples were immersed at 65-70°C for 30 minutes. Make sure you have a thermometer to check the temperature of the solution before proceeding with immersion of the pieces.
**Caution!** PLA pieces dipped at temperatures above 80°C for long times might face structural damage, as high temperatures compromise their mechanical strength. PETG samples are more heat-resistant than PLA, and temperatures of 80-100°C (which are typical of dyeing processes) do not compromise their mechanical properties.
Both white and pale pink (skin tone) PLA filaments were used for the test. Pale pink and dark pink PETG were also tested, with the latter resulting in negligible coloration after immersion in the dye solution (results about dyeing test of dark pink PETG samples will not be reported in this section).  
We recommend using white or light pink filaments to achieve a final skin-like colour of various intensities/tones, as dark colours are more difficult to dye.
The filaments used in the test are: PLA (white and skin colour, by WarHorse 3D filament), PETG (skin colour, by 3D Druker Filament, code 3DZPETG175Skn). 

As these pieces tend to float, we recommend stirring the solution and turning over the plastic components several times in order to have an homogeneous coloration of the samples.

After dyeing, some components were dipped in water for 40 minutes to test the discoloration of the samples.

##Results

*Table 1. Variation of colour of PLA and PETG samples dipped for 30 minutes in dyeing solutions of different concentrations. In the first row, pictures of pristine (not dyed) samples are reported.*

| Dying solution | PLA (skin colour) | PLA (white) | PETG (skin colour) |
|:--             |:--                |:--          |:--                 |
|Pristine| ![plap pristine](img/plap_pristine.png) | ![plaw pristine](img/plaw_pristine.png) | ![petg pristine](img/petg_pristine.png) |
|2.5ml dye in 0.3L water (0.8% vol.)| ![2.5 plap](img/2.5_plap.jpeg) | ![2.5 plaw](img/2.5_plaw.jpeg) | ![2.5 petg](img/2.5_petg.jpeg) |
|5ml dye in 0.3L water (1.6% vol.)| ![5 plap](img/5_plap.jpeg) | ![5 plaw](img/5_plaw.jpeg) | ![5 petg](img/5_petg.jpeg) |
|10ml dye in 0.3L water (3.2% vol.)| ![10 plap](img/10_plap.jpeg) | ![10 plaw](img/10_plaw.jpeg) | ![10 petg](img/10_petg.png) |
|15ml dye in 0.3L water (4.8% vol.)| ![15 plap](img/15_plap.jpeg) | ![15 plaw](img/15_plaw.jpeg) | ![15 petg](img/15_petg.jpeg) |

*Table 2. Colour tones of PLA and PETG samples dipped for 30 minutes in dyeing solutions of different concentrations. HEX (hexadecimal) and RGB (red green blue) codes are reported for each colour in the corresponding box.*

![color tones table](img/dyeing_tab2.jpg)

Different colours can be achieved by increasing the concentration of the dyeing solution.
Light skin tones can be easily obtained starting from a white filament. For darker tones, starting from a light pink base colour is recommended; as an alternative strategy to achieve darker tones, further increase of the dye concentration can be tested.  
To obtain specific tones, refer to the RitDye [colouring guide](https://www.ritdye.com/color-formulas/?type=203&hue=0&collection=0&collaboration=0), where suggested combinations of dyeing products and the resulting nuances are reported.  
Immersion of dyed samples in water at room temperature for 40 minutes did not cause any visible discoloration, nor deterioration of the mechanical properties of the 3D-printed objects.
Some absorption of water during the process is however hypothesised, which might induce mechanical deterioration of the components in the long terms, therefore we recommend carefully drying the pieces after the dyeing process.
