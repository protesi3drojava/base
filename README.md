# 3D Printed Prothesis for Rojava

We are engaging the production of low-cost prothesis using 3D printing, as a technology to be transferred to the North East of Syria.  
The project, active for two years, proposes to experiment in Italy the low cost production of prothesis for upper limbs and prothesis components for lower limbs.

## Goals and Objectives

We currently are:  
- Creating technical and experimental documentation, pubblic and accessible;
- Evaluating, planning and carrying out technological transfer at the Qamislo Orthopedic Laboratory, in the North East of Syria, to support the local production of prothesis.

Our objectives are:  
- Activating specific training for students of the Qamislo Medical Academy;
- Undertaking collaboration with other entities interested in implementing the process.

## The Covid Pandemic

As part of the local community, in addition to printing prothesis components, we also printed other medical devices, while, during the Covid19 emergency, we joined the call of the University of Tor Vergata, producing protective visors for hospitals.

## Links

[Facebook page](https://www.facebook.com/Rojava3DProsthetics)  
[Staffetta Sanitaria's project](http://www.staffettasanitaria-rojava.it/progetto-protesi-3d/)  
[Ingegneria Senza Frontiere's article](http://www.isf-roma.org/le-protesi-degli-arti-superiori-realizzate-con-stampanti-3d/)  
